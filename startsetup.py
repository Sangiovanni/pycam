import cv2
import os
from views2 import Sstart
CAM_NUM = -1

ss = None

def start(device_name, cam_num):

    global ss

    ss = Sstart(device_name, cam_num)

    while (1):
        ss.cap.captureFrame()
        image = ss.cap.getOriginal()

        ###################################################
        inp_char = cv2.waitKey(1)
        # print(inp_char)
        if inp_char == 27:  # ESC
            ss.programQuit()
            break
        elif inp_char == ord('q') or inp_char == ord('Q'):
            ss.confSave()
            ss.programQuit()
            break
        elif inp_char == ord('d') or inp_char == ord('D'):
            ss.confReload()
        elif inp_char == ord('r') or inp_char == ord('R'):
            ss.rectangleDelete()
        elif inp_char == ord('s') or inp_char == ord('S'):
            ss.segmentDelete()
        elif cam_num!=-1 and (inp_char == ord('i') or inp_char == ord('I')):
            ss.cap.saveImage(image)
        elif cam_num == -1:    #input from saved image
            if inp_char == ord('+'):
                ss.cap.getNextFile()
            elif inp_char == ord('-'):
                ss.cap.getPrevFile()

        ######################################################
        gray_image = ss.cap.getGrayScale()
        out_image = image.copy()
        out_image = ss.draw.drawRect(out_image, ss.setup.getP0(), ss.setup.getP1())
        out_image = ss.draw.drawSegment(out_image, ss.setup.getSegmentCoord(ss.display.selected_display, ss.display.selected_segment))
        cv2.threshold(gray_image, ss.setup.getThreshold(), 255, cv2.THRESH_BINARY, gray_image)
        cv2.imshow('Binary', gray_image)
        cv2.imshow(device_name, out_image)



#menu
while(1):
    print("\n\nWelcome to device configuration program")
    print("\nFor a new device, create an empty folder that has the same name as the device model. Then restart the program")
    print("\nTo modify an existing configuration, type the number corresponding to the device listed. Than press enter:")
    listdevices = os.listdir("devices")
    for i in range(len(listdevices)):
        print("[" + str(i) + "] " + listdevices[i])
    #inp_char = 0 
    inp_char = int(input(">>>"))
    if inp_char >=0 and inp_char < len(listdevices):
        start(listdevices[inp_char], CAM_NUM)
        break
    else:
        print("Invalid choice!")

