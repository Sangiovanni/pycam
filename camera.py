import cv2
import os

class Camera:
    def __init__(self, cam_num, pathdir):
        self.cam_num = cam_num
        self.pathdir = pathdir
        if(self.cam_num != -1):
            self.cap = cv2.VideoCapture(cam_num, cv2.CAP_DSHOW)
        else:
            self.file_index = 1
            if os.path.exists(self.pathdir + '/images') and self.getNumFilesInDir(self.pathdir + "/images/") >0:
                self.last_frame = cv2.imread(self.pathdir + "/images/bild_1.png", cv2.IMREAD_ANYCOLOR)
            else:
                self.last_frame = cv2.imread('welcome.png', cv2.IMREAD_ANYCOLOR)

    def saveImage(self, img):
        if not os.path.exists(self.pathdir + '/images'):
            os.makedirs(self.pathdir + '/images')
        dir = self.pathdir + "/images/"
        filename = dir + "bild_"+ str(len(os.listdir(dir)) +1)+ ".png"
        cv2.imwrite(filename, img)

    def getNextFile(self):
        if (self.cam_num == -1):
            if os.path.exists(self.pathdir + '/images'):
                num_files = self.getNumFilesInDir(self.pathdir +"/images/")
                if (self.file_index < num_files):
                    self.file_index = self.file_index + 1
                    self.last_frame = cv2.imread(self.pathdir +'/images/bild_' + str(self.file_index) +'.png', cv2.IMREAD_ANYCOLOR)

    def getPrevFile(self):
        if (self.cam_num == -1):
            if (self.file_index != 1):
                self.file_index = self.file_index - 1
                self.last_frame = cv2.imread(self.pathdir +'/images/bild_' + str(self.file_index) +'.png', cv2.IMREAD_ANYCOLOR)

    def getNumFilesInDir(self,dir):
        count = 0
        for path in os.listdir(dir):
            if os.path.isfile(os.path.join(dir, path)):
                count += 1
        return count

    def captureFrame(self):
        if (self.cam_num != -1):
           self.last_frame = self.cap.read()
        return self.last_frame

    def getOriginal(self):
        return self.last_frame

    def getGrayScale(self):
        self.gray_scale = cv2.cvtColor(self.last_frame, cv2.COLOR_BGR2GRAY)
        return self.gray_scale

    def release(self):
        if (self.cam_num != -1):
            self.cap.release()