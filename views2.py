import cv2
import numpy as np
from settings2 import Setup
from display2 import Display
from draw import Draw
from camera import Camera
from analysis import Analysis

displays = {'d5': (55, 25), 'd4': (100, 24), 'd3': (145, 24), 'd2': (192, 25), 'd1': (241, 24)}
segments = {'a': (146, 75), 'b': (193, 115), 'c': (194, 196), 'd': (144, 238), 'e': (98, 196), 'f': (98, 113),
				'g': (146, 155), 'dp': (230, 234)}

class Ustart():

	def __init__(self, device_name, cam_num):
		global displays
		global segments
		self.device_name = device_name
		self.cam_num = cam_num
		self.setup = Setup(device_name)
		self.cap = Camera(cam_num, "devices/" + device_name)
		self.draw = Draw()
		self.windowsOpen()
		self.analyse = Analysis(self.setup.dat, displays, segments)

	def windowsOpen(self):
		cv2.namedWindow(self.device_name)
		cv2.moveWindow(self.device_name, 642, 0)

		# create window function keys description
		options = ["ESC: Program quit"]
		self.optionsShow("Setup", options)

	def optionsShow(self, windows_name, options):
		cv2.namedWindow(windows_name)
		cv2.moveWindow(windows_name, 0, 520)
		if self.cam_num != -1:
			options.append("I: Save image")
		if self.cam_num == -1:
			options.append("+/-: Switch image")
		blank_image = np.ones(shape=[200, 350, 3], dtype=np.uint8)
		for i in range(len(options)):
			cv2.putText(blank_image, options[i], (3, 25 * (1 + i)), cv2.FONT_HERSHEY_SIMPLEX, 0.5, (0, 255, 0), 1,
						cv2.LINE_AA, False)
		cv2.imshow(windows_name, blank_image)

	def readFromImage(self, grayimg):
		return self.analyse.displayRead(grayimg)


	def programQuit(self):
		self.cap.release()
		cv2.destroyAllWindows()


class Sstart(Ustart):  # Start setup view

	mouse_btn_is_clicked = False

	def __init__(self, device_name, cam_num):
		super().__init__(device_name, cam_num)
		self.display = Display()
		self.windowsOpen()

	def windowsOpen(self):
		cv2.namedWindow("Binary")
		cv2.moveWindow("Binary", 0, 0)
		cv2.namedWindow(self.device_name)
		cv2.moveWindow(self.device_name, 642, 0)

		# create window function keys description
		options = ["Q: Save and quit", "ESC: Quit without saving", "D: Discard changes",
				   "R: Delete rectangle", "S: Delete display segment"]
		self.optionsShow("Setup", options)

		cv2.createTrackbar("Threshold_bar", "Setup", self.setup.getThreshold(), 255, self.setup.setThreshold)
		cv2.setMouseCallback(self.device_name, self.setCoord)

	def confReload(self):
		self.setup.loadFromFile()

	def confSave(self):
		self.setup.onFileSave()

	def rectangleDelete(self):
		self.setup.resetRectCoord()

	def segmentDelete(self):
		self.setup.delSegmentCoord(self.display.selected_display, self.display.selected_segment)

	def setCoord(self, event, x, y, flags, param):

		if event == cv2.EVENT_LBUTTONDOWN:
			self.mouse_btn_is_clicked = True
			if self.setup.getP1() == (0, 0) or self.setup.getP0() == (0, 0):
				self.setup.setRectCoord((x, y))
				self.mouse_btn_is_clicked = False
				return
		if self.mouse_btn_is_clicked:
			self.setup.setSegmentCoord(self.display.selected_display, self.display.selected_segment, (x, y))
		if event == cv2.EVENT_LBUTTONUP:
			self.mouse_btn_is_clicked = False
			return



