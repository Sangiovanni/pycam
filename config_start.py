#config_start.py
from camera import Camera
from views import ConfigWindowsStart
from display import Display
from settings import Setup
import cv2

Setup.getSettingFromFile()
ConfigWindowsStart()
cap = Camera(0)
disp = Display(True)   #True : wiew enabled   False: view disabled

while(1):
    cap.captureFrame()
    image = cap.getOriginal()
    gray_image = cap.getGrayScale()

    inp_char = cv2.waitKey(1)
    #print(inp_char)
    if inp_char == 27:   #ESC
        cap.release()
        cv2.destroyAllWindows()
        break
    elif inp_char == ord('s') or inp_char == ord('S'):
        Setup.onFileSave()
    elif inp_char == ord('r') or inp_char == ord('R'):
        Setup.resetRectCoord()
    elif inp_char == ord('t') or inp_char == ord('T'):
        Setup.deleteSegment()
    elif inp_char == ord('f') or inp_char == ord('F'):
        Setup.saveImage(image)
    elif inp_char == 45:  # -
        cap.getPrevFile()
    elif inp_char == 43:  # +
        cap.getNextFile()

    image = Setup.drawRect(image)
    image = Setup.drawSegment(image)
    cv2.threshold(gray_image, Setup.getThreshold(), 255, cv2.THRESH_BINARY, gray_image)
    cv2.imshow('Binary', gray_image)
    cv2.imshow('Original', image)
    disp.imgUpdate()
