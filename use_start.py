from views2 import Ustart
import os
import cv2
CAM_NUM = -1

us = None

def start(device_name, cam_num):

    global us

    us = Ustart(device_name, cam_num)

    while (1):
        us.cap.captureFrame()
        image = us.cap.getOriginal()

        ###################################################
        inp_char = cv2.waitKey(1)
        # print(inp_char)
        if inp_char == 27:  # ESC
            us.programQuit()
            break
        elif inp_char == ord('h') or inp_char == ord('H'):
            gray_image = us.cap.getGrayScale()
            print(us.readFromImage(gray_image))
        elif inp_char == ord('q') or inp_char == ord('Q'):
            us.programQuit()
            break
        elif cam_num!=-1 and (inp_char == ord('i') or inp_char == ord('I')):
            us.cap.saveImage(image)
        elif cam_num == -1:    #input from saved image
            if inp_char == ord('+'):
                us.cap.getNextFile()
            elif inp_char == ord('-'):
                us.cap.getPrevFile()

        ######################################################
        out_image = image.copy()
        out_image = us.draw.drawRect(out_image, us.setup.getP0(), us.setup.getP1())
        cv2.imshow(device_name, out_image)




#menu
while(1):
    print("\n\nWelcome to device use program")
    print("\nSelect the number corresponding to the device listed. Than press enter:")
    listdevices = os.listdir("devices")
    for i in range(len(listdevices)):
        print("[" + str(i) + "] " + listdevices[i])
    #inp_char = 0
    inp_char = int(input(">>>"))
    if inp_char >=0 and inp_char < len(listdevices):
        start(listdevices[inp_char], CAM_NUM)
        break
    else:
        print("Invalid choice!")

