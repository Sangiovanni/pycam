import json
import os

class Setup():
    dat = {'Threshold': 128, 'rect_P0': (0, 0), 'rect_P1': (0, 0), 'digits':{}} #default values

    def __init__(self, device_name):
        self.device_path = "devices/" + device_name + "/conf.json"
        self.loadFromFile()

    def onFileSave(self):   #save the variable dat on file
        with open(self.device_path, 'w') as json_file:
            json.dump(self.dat, json_file)

    def loadFromFile(self):
        if os.path.exists(self.device_path):
            with open(self.device_path, 'r') as json_file:
                self.dat = json.load(json_file)

    #THRESHOLD SETTINGS
    def setThreshold(self, value):
        self.dat['Threshold'] = value

    def getThreshold(self):
        return self.dat['Threshold']

    #RECTANGLE COORD SETTINGS
    def getP0(self):   #coord upper left corner
        return self.dat['rect_P0']

    def getP1(self):   #coord lower right corner
        return self.dat['rect_P1']

    def setRectCoord(self, coord):  #set the first time P0 and the second time P1
        if self.dat['rect_P0'] == (0, 0):
            self.dat['rect_P0'] = coord
        elif self.dat['rect_P1'] == (0, 0):
            self.dat['rect_P1'] = coord

    def resetRectCoord(self):
        self.dat['rect_P0'] = self.dat['rect_P1'] = (0, 0)

    #SEGMENT COORD SETTINGS
    def setSegmentCoord(self, digit, segment, coord):
        coord = tuple(coord)
        if not digit in self.dat['digits']:
            self.dat['digits'][digit] = {}
        if not segment in self.dat['digits'][digit]:
            self.dat['digits'][digit][segment] = []
        if (self.dat['digits'][digit][segment]).count(coord) == 0:
            (self.dat['digits'][digit][segment]).append(coord)

    def delSegmentCoord(self, digit, segment):
        if digit in self.dat['digits']:
            if segment in self.dat['digits'][digit]:
                del self.dat['digits'][digit][segment]

    def getSegmentCoord(self, digit, segment):
        retval = []
        if digit in self.dat['digits']:
            if segment in self.dat['digits'][digit]:
                retval = self.dat['digits'][digit][segment]
        return retval

