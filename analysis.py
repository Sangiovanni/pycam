

class Analysis():
	map = {
		'hhhhhhhh': '',
		'hhhhhhhd': '.',
		'ddddddhh': '0',
		'ddddddhd': '0.',
		'hddhhhhh': '1',
		'hddhhhhd': '1.',
		'ddhddhdh': '2',
		'ddhddhdd': '2.',
		'ddddhhdh': '3',
		'ddddhhdd': '3.',
		'hddhhddh': '4',
		'hddhhddd': '4.',
		'dhddhddh': '5',
		'dhddhddd': '5.',
		'dhdddddh': '6',
		'dhdddddd': '6.',
		'dddhhhhh': '7',
		'dddhhhhd': '7.',
		'dddddddh': '8',
		'dddddddd': '8.',
		'ddddhddh': '9',
		'ddddhddd': '9.',
	}

	def __init__(self, dat, digits, segments):
		self.displays = dat['digits']
		self.digits = digits
		self.segments = segments
		self.thr = dat['Threshold']
		
	def isSegmentDark(self, gray_image, digit, segment):
		if digit not in self.displays or segment not in self.displays[digit]:
			return False
		cnt_hell_pixels = 0
		cnt_dark_pixels = 0

		for p in self.displays[digit][segment]:
			if gray_image[ p[1], p[0] ] <= self.thr:
				cnt_dark_pixels += 1
			else:
				cnt_hell_pixels += 1
		return cnt_dark_pixels > cnt_hell_pixels
		
	def displayRead(self, gray_image):
		issue = ''
		for digit in self.digits:
			keymap = ''
			for segment in self.segments:
				if self.isSegmentDark(gray_image, digit, segment):
					keymap += 'd'
				else:
					keymap += 'h'
			if keymap in self.map:
				issue = issue + self.map[keymap]
			else:
				issue += '?'
		return issue
					