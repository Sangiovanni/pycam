import cv2
import math
import copy

selected_segment = 'a'
selected_display = 'd2'

class Display():

    def __init__(self, en):
        self.displays = {'d1': (31, 25), 'd2': (76, 24), 'd3': (121, 24), 'd4': (167, 25), 'none': (217, 24)}
        self.segments = {'a': (146, 75), 'b': (193, 115), 'c': (194, 196), 'd': (144, 238), 'e': (98, 196), 'f': (98, 113), 'g': (146, 155), 'dp': (230, 234)}
        if (en == True):
            self.img = cv2.imread('display1.png', cv2.IMREAD_COLOR)
            cv2.namedWindow("digit")
            cv2.setMouseCallback('digit', self.setSelectedDisplayAndSegment)
            cv2.imshow('digit', self.img)

    def getSelectedSegment(self):
        global selected_segment
        return selected_segment

    def getSelectedDisplay(self):
        global selected_display
        return selected_display

    def imgUpdate(self):
        global selected_segment
        global selected_display
        imgcopy = copy.copy(self.img)

        #update view radiobutton segment
        for key in self.segments:
            if selected_segment == key:
                thick = 3
            else:
                thick = 1
            cv2.circle(imgcopy, self.segments[key], 14, (25, 100, 80), thick)

            # update view radiobutton display
            for key in self.displays:
                if selected_display == key:
                    thick = 3
                else:
                    thick = 1
                cv2.circle(imgcopy, self.displays[key], 20, (255, 255, 255), thick)

        cv2.imshow('digit', imgcopy)

    def setSelectedDisplayAndSegment(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # print("x : % 3d, y : % 2d" %(x, y))
            self.setSelectedDisplay(event, x, y, flags, param)
            self.setSelectedSegment(event, x, y, flags, param)

    def setSelectedSegment(self, event, x, y, flags, param):
        global selected_segment
        global selected_display

        if selected_display != 'none':
            for key in self.segments:
                dist = self.get_point_distance((x, y), self.segments[key])
                if dist < 14:
                    selected_segment = key
                    break

    def setSelectedDisplay(self, event, x, y, flags, param):
        global selected_segment
        global selected_display
        #if event == cv2.EVENT_LBUTTONDOWN:
        for key in self.displays:
            dist = self.get_point_distance((x, y), self.displays[key])
            if dist < 20:
                selected_display = key
                selected_segment = ''
                break

    def get_point_distance(self, a, b):
         return math.sqrt( math.pow(a[0]-b[0], 2)  + math.pow(a[1]-b[1], 2) )
