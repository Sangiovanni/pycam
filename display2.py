import cv2
import math
import copy

#This Class displays the image of the digit with radiobuttons.
class Display():
    selected_display = 'd1' #radiobutton selected by default for the digit's position
    selected_segment = 'a'  # radiobutton selected by default for the segment
    #coordinates of radiobutton ((central pixel))
    displays = {'d5':(55, 25), 'd4':(100, 24),'d3':(145, 24), 'd2':(192, 25), 'd1':(241, 24)}
    segments = {'a':(146, 75), 'b':(193, 115), 'c':(194, 196), 'd':(144, 238), 'e':(98, 196), 'f':(98, 113), 'g':(146, 155), 'dp':(230, 234)}

    def __init__(self):
        self.img = cv2.imread('display3.png', cv2.IMREAD_COLOR)
        cv2.namedWindow("digit")
        cv2.setMouseCallback('digit', self.setSelectedDisplayAndSegment)
        self.imgUpdate() #show digit' s window with default selected radio-btns

    def setSelectedDisplayAndSegment(self, event, x, y, flags, param):
        if event == cv2.EVENT_LBUTTONDOWN:
            # print("x : % 3d, y : % 2d" %(x, y))
            self.setSelectedDisplay(event, x, y, flags, param) #act nur if the click is on a related radio button
            self.setSelectedSegment(event, x, y, flags, param) #act nur if the click is on a related radio button
            self.imgUpdate()

    def setSelectedSegment(self, event, x, y, flags, param):
        for key in self.segments:
            dist = self.get_point_distance((x, y), self.segments[key])
            if dist < 14: #radiobutton 's ray
                self.selected_segment = key
                break

    def setSelectedDisplay(self, event, x, y, flags, param):
        #if event == cv2.EVENT_LBUTTONDOWN:
        for key in self.displays:
            dist = self.get_point_distance((x, y), self.displays[key])
            if dist < 20: #radiobutton 's ray
                self.selected_display = key
                break

    def get_point_distance(self, a, b):
         return math.sqrt(math.pow(a[0]-b[0], 2) + math.pow(a[1]-b[1], 2))

    def imgUpdate(self):
        imgcopy = copy.copy(self.img)
        #update view radiobutton segment
        for key in self.segments:
            thick = 3 if self.selected_segment == key else 1
            cv2.circle(imgcopy, self.segments[key], 14, (25, 100, 80), thick)
            # update view radiobutton display
            for key in self.displays:
                thick = 3 if self.selected_display == key else 1
                cv2.circle(imgcopy, self.displays[key], 20, (255, 255, 255), thick)
        cv2.imshow('digit', imgcopy)

if __name__ == "__main__":
    Display()
    while (1):
        inp_char = cv2.waitKey(1)
        # print(inp_char)
        if inp_char == 27:  # ESC
            cv2.destroyAllWindows()
            break

